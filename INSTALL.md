# CmDock build instructions

CmDock uses the [Meson](https://mesonbuild.com) build system.

## Prerequisites

- C/C++ compiler
- Python 3
- [Ninja](https://github.com/ninja-build/ninja/releases) (comes packed with the Meson installer for Windows)
- [Meson](https://github.com/mesonbuild/meson/releases)

### Linux

```bash
sudo apt install gcc gdb g++
sudo apt install pkg-config cmake
sudo apt install python3 python3-pip python3-setuptools python3-wheel
sudo apt install ninja-build
pip3 install --user meson
```

### macOS

We recommend using [Homebrew](https://brew.sh) for installing prerequisites

```bash
brew install gcc gdb g++
brew install pkg-config cmake
brew install python3 python3-pip python3-setuptools python3-wheel
brew install ninja-build
pip3 install --user meson
```

### Windows 10 / 11

- Download and run the [Meson installer](https://github.com/mesonbuild/meson/releases/) (includes Ninja)
- If you will be using [MinGW](http://www.mingw.org/) that includes a port of GCC compiler, you need to add this to PATH:

  `C:\path\to\mingw64\`

  `C:\path\to\mingw64\bin`

- If you wish to use the **msvc** compiler, you need to run Meson under "Visual Studio command prompt" like "x64 Native Tools Command Prompt for VS 2019".

- If you wish to use the **clang** compiler (bundled with Intel oneAPI, for instance), you need to run Meson under "Intel oneAPI Tools command prompt" and select the compiler before invoking Meson with `set CXX=clang-cl`.

## Build

1. Setup

    `meson setup <builddir> --buildtype <debug|debugoptimized|release> --prefix </absolute/install/path/>`

    Add `--wipe` to start from the scratch.

    | buildtype | meaning |
    | ------ | ------ |
    | debug| debug info is generated but the result is not optimized (default)|
    | debugoptimized  | debug info is generated and the code is optimized (on most compilers this means -g -O2)|
    | release         | full optimization (O3), no debug info|

    For setting optimization levels and toggling debug, you can either set the `buildtype` option, or you can set the `optimization` and `debug` options which give finer control over the same. Whichever you decide to use, the other will be deduced from it. For example, `--buildtype debugoptimized` is the same as `-Ddebug=true -Doptimization=2` and vice-versa.

    You can set a compiler by setting an environment variable (`CC` for C and `CXX` for C++), for example:

    `CC=clang CXX=clang++ meson ...` on Linux

    `set CC=clang-cl && set CXX=clang-cl && meson ...` on Windows

    See [Meson docs](https://mesonbuild.com/howtox.html) for other options.

2. Compile

    `meson compile -C <builddir>`

    Add `--clean` to clean the build folder.

3. Install

    `meson install -C <builddir>`

    This deploys executables and other required files (data, scripts) to the folder specified by `--prefix`.

    Alternatively, you can specify the installation folder using `DESTDIR`:

    `DESTDIR=/path/to/staging/area meson install -C <builddir>`

    or on Windows:

    `set DESTDIR=/path/to/staging/area && meson install -C <builddir>`

    To install without rebuild or/and changes only:

    `meson install -C <builddir> --no-rebuild --only-changed`

## Environment setup

### Linux and macOS

```bash
export CMDOCK_ROOT=/path/to/cmdock
export PATH=$CMDOCK_ROOT/build:$PATH
export PATH=$CMDOCK_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$CMDOCK_ROOT/build:$LD_LIBRARY_PATH
export PERL5LIB=$CMDOCK_ROOT/lib:$PERL5LIB
```

### Windows

```bat
setx CMDOCK_ROOT "C:\path\to\cmdock"
setx PATH "%PATH%;C:\path\to\cmdock\build"
setx PATH "%PATH%;C:\path\to\cmdock\bin"
setx PERL5LIB "%PERL5LIB%;C:\path\to\cmdock\lib"
setx LD_LIBRARY_PATH "%LD_LIBRARY_PATH%;C:\path\to\cmdock\lib"
```

**Note**: `setx` has problems with variables larger than 1024 characters. To overcome this limitation, you can use the following PowerShell script.

<details>
<summary>PowerShell script (click to expand)</summary>

```powershell
$rbtroot = 'C:\cmdock'
$path1 = $rbtroot + '\bin'
$path2 = $rbtroot + '\build'
$path3 = $rbtroot + '\lib'
$paths = @($rbtroot, $path1, $path2, $path2, $path3 )
$envs = @('CMDOCK_ROOT', 'PATH', 'PATH', 'LD_LIBRARY_PATH', 'PERL5LIB')

For ($i=0; $i -lt $paths.Length; $i++) {
    $new_entry = $paths[$i]
    $search_pattern = ';' + $new_entry.Replace("\","\\")

    $old_path = [Environment]::GetEnvironmentVariable($envs[$i], 'User');
    if(!$old_path){
        $new_path = $new_entry
    }
    else{
        $replace_string = ''
        $without_entry_path = $old_path -replace $search_pattern, $replace_string
        $new_path = $without_entry_path + ';' + $new_entry
    }
    [Environment]::SetEnvironmentVariable($envs[$i], $new_path, 'User');
}
```

</details>

## Integration with Visual Studio Code

Example of `launch.json` and `tasks.json` using MinGW on Windows:

### launch.json

```javascript
{
    "configurations": [
        {
            "name": "Debug with Meson",
            "type": "cppdbg",
            "request": "launch",
            "MIMode": "gdb",
            "miDebuggerPath": "C:\\path\\to\\mingw-w64\\mingw64\\bin\\gdb.exe",
            "stopAtEntry": false,
            "cwd": "${workspaceRoot}",
            "program": "${workspaceRoot}\\builddir-debug\\${fileBasenameNoExtension}.exe",
            "args": [],
            "setupCommands": [
                {
                    "description": "Enable pretty-printing for gdb",
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                }
            ],
            "preLaunchTask": "Meson: build debug"
        }
    ]
}
```

### tasks.json

```json
{
    "tasks": [
        {
            "label": "Meson: build debug",
            "type": "shell",
            "command": "meson setup builddir-debug --buildtype=debug && cd builddir-debug && ninja"
        }
    ],
    "version": "2.0.0"
}
```

## Integration with Visual Studio

Tested with Visual Studio 2019.

### Configure Visual Studio project files with Meson

According to the Meson [guide](https://mesonbuild.com/Using-with-Visual-Studio.html):

1. Run "x64 Native Tools Command Prompt for VS 2019"

2. Invoke the Meson setup using switch `--backend vs`, e.g.:

    `meson setup builddir-debug --buildtype debug --backend vs2019 --prefix %cd%\deploy\debug`

    or/and

    `meson setup builddir-release --buildtype release --backend vs2019 --prefix %cd%\deploy\release`

3. Open the configured solution file, e.g., `builddir-debug\CmDock.sln`, in Visual Studio.

4. Configure the following project property due to [two-phase name lookup error](https://stackoverflow.com/questions/56782470/why-do-i-have-warning-c4199-two-phase-name-lookup-is-not-supported-for-c-cli):

    Project &rarr; Properties &rarr; C/C++ &rarr; Command line &rarr; Add `/Zc:twoPhase-` to the Additional Options text-field

5. [_optional_] Enable multi-processor compilation:

    Project &rarr; Properties &rarr; General &rarr; Multi-processor Compilation &rarr; Yes

### External build

See [Meson docs](https://mesonbuild.com/Vs-External.html).
