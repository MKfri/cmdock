#!/bin/sh

SRC_DIR=$(dirname "$(dirname "${PWD}")")
NDK_ROOT="${HOME}/android-sdk/ndk/toolchains/llvm/prebuilt/linux-x86_64"
PATH="${NDK_ROOT}/bin:${PATH}"

abi=${1}
case ${abi} in
	armeabi-v7a)
		target="armv7-none-linux-androideabi"
		;;
	arm64-v8a)
		target="aarch64-linux-android"
		;;
	*)
		echo "Usage: ./android-cmdock.sh <armeabi-v7a|arm64-v8a>"
		exit 1
	;;
esac

meson_cargs="--sysroot=${NDK_ROOT}/sysroot --target=${target}"
meson_cargs="${meson_cargs} -fPIE -fPIC -Wno-c++11-narrowing"
meson_ldargs="-static-libstdc++ -fopenmp -static-openmp"

rm -rf "build-${abi}" "image-${abi}"
mkdir "build-${abi}"
cd "build-${abi}" || exit 1
meson "${SRC_DIR}" \
	--cross-file "${SRC_DIR}/contrib/cross-files/linux-android-${abi}.cross" \
	--prefix "${SRC_DIR}/contrib/android/image-${abi}" \
	-Dbuildtype=release \
	-Dc_args="${meson_cargs}" \
	-Dc_link_args="${meson_ldargs}" \
	-Dcpp_args="${meson_cargs}" \
	-Dcpp_link_args="${meson_ldargs}"
ninja install
